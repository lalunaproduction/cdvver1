package x.android.cdv.presenter.widget;

/**
 * Created by ndvu on 7/12/17.
 */


import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import x.android.cdv.R;
import x.android.cdv.view.activities.MainActivity;
import x.android.cdv.view.activities.screen.DrawerActivity;

public class Drawer extends Fragment {
    private static String TAG = Drawer.class.getSimpleName();

    private ListView listView;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
//    private DNPMenuListAdapter dnpMenuListAdapter;
//    private ArrayList<DNPMenuItem> navDrawerItems;
    private View containerView;
    private CharSequence mTitle;
    private CharSequence mDrawerTitle;
    private String[] navMenuTitles;
    // slide menu items
    private TypedArray navMenuIcons;

    public Drawer() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View layout = inflater.inflate(R.layout.fragment_drawer, container, false);

        mTitle = getResources().getString(R.string.dnp_category);
        mDrawerTitle = getResources().getString(R.string.app_name);
        // load slide menu items
//        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
        // nav drawer icons from resources
//        navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);
//        navDrawerItems = new ArrayList<>();
        listView = (ListView) layout.findViewById(R.id.list_slider_menu);
        return layout;
    }

    public void initDrawer(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.app_name, R.string.all_reminder_title) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                ((DrawerActivity) getActivity()).setTitleBar(mDrawerTitle);
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                ((DrawerActivity) getActivity()).setTitleBar(mTitle);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                toolbar.setAlpha(1 - slideOffset / 2);
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }

    /**
     * Displaying fragment view for selected nav drawer list item
     */
    private void displayView(int position) {
        // update the main content by replacing fragments
        Fragment fragment = null;
//        switch (position) {
//            case 0:
//                mTitle = getResources().getString(R.string.user_overview);
//                fragment = new MainFragment();
//                break;
//            case 1:
//                mTitle = getResources().getString(R.string.user_weather);
//                fragment = new MainFragment();
//                break;
//            case 2:
//                mTitle = getResources().getString(R.string.user_bank_atm);
//                fragment = new MainFragment();
//                break;
//            case 3:
//                mTitle = getResources().getString(R.string.user_atm);
//                fragment = new MainFragment();
//                break;
//            case 4:
//                mTitle = getResources().getString(R.string.user_taxi);
//                fragment = new MainFragment();
//                break;
//            case 5:
//                mTitle = getResources().getString(R.string.user_embassy);
//                fragment = new MainFragment();
//                break;
//            case 6:
//                mTitle = getResources().getString(R.string.user_move);
//                fragment = new MainFragment();
//                break;
//            case 7:
//                mTitle = getResources().getString(R.string.user_learn);
//                fragment = new MainFragment();
//                break;
//            default:
//                break;
//        }
//        if (fragment != null) {
//            final Fragment finalFragment = fragment;
//            final FragmentManager fragmentManager = getActivity().getSupportFragmentManager(); //getFragmentManager();
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    fragmentManager.beginTransaction()
//                            .replace(R.id.container_body, finalFragment, null)
//                            .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE).addToBackStack(null).commit();
//                }
//            }, 300);
//
//
//            listView.setItemChecked(position, true);
//            mDrawerLayout.closeDrawers();
//        } else {
//            Log.e("MainActivity", "Error in creating fragment");
//        }
    }

    public void createNavigatorItems() {
//        if (navDrawerItems.size() == 0) {
//            // adding nav drawer items to array
//            // Tong Quan
//            navDrawerItems.add(new DNPMenuItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
//            // Thoi Tiet
//            navDrawerItems.add(new DNPMenuItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
//            // Taxi
//            navDrawerItems.add(new DNPMenuItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
//            // Bank
//            navDrawerItems.add(new DNPMenuItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
//            // ATM
//            navDrawerItems.add(new DNPMenuItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
//            // Dai Su Quan
//            navDrawerItems.add(new DNPMenuItem(navMenuTitles[5], navMenuIcons.getResourceId(5, -1)));
//            // Di chuyen
//            navDrawerItems.add(new DNPMenuItem(navMenuTitles[6], navMenuIcons.getResourceId(6, -1)));
//            // Hoc tieng Viet
//            navDrawerItems.add(new DNPMenuItem(navMenuTitles[7], navMenuIcons.getResourceId(7, -1)));
//        }
//        if (navDrawerItems.size() != 0) {
//            // Recycle the typed array
//            navMenuIcons.recycle();
//
//            listView.setOnItemClickListener(new SlideMenuClickListener());
//            // setting the nav drawer list dnpMenuListAdapter
//            dnpMenuListAdapter = new DNPMenuListAdapter(getContext(), navDrawerItems);
//            listView.setAdapter(dnpMenuListAdapter);
//        }
    }

    /**
     * Slide menu item click listener
     */
    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // display view for selected nav drawer item
            displayView(position);
        }
    }
}
