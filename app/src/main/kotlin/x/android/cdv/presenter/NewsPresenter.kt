package x.android.cdv.presenter

import x.android.cdv.model.NewsItem
import x.android.cdv.service.CDVApi
import x.android.cdv.view.interfaces.NewsView
import io.realm.Realm
import rx.Observable
import rx.Subscription
import rx.android.schedulers.AndroidSchedulers


/**
 * @author VuMap
 */
class NewsPresenter {

    private var mNewsApiInterface: CDVApi? = null
    private var mNewsView: NewsView? = null

    private var subscription: Subscription? = null

    fun onViewCreated(view: NewsView) {
        mNewsView = view
    }

    fun setNewsApiInterface(newsApiInterface: CDVApi) {
        this.mNewsApiInterface = newsApiInterface
    }

    fun loadNews() {
        subscription = mNewsApiInterface!!
                .getNews()
                .map { it.newsItem }
                .flatMap({ items ->
                    Realm.getDefaultInstance().executeTransaction({ realm ->
                        realm.delete(NewsItem::class.java)
                        realm.insert(items)
                    })
                    Observable.just(items)
                })
                .onErrorResumeNext { _ ->
                    val realm = Realm.getDefaultInstance()
                    val items = realm.where(NewsItem::class.java).findAll()
                    Observable.just(realm.copyFromRealm(items))
                }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnTerminate { mNewsView?.hideLoading() }
                .subscribe({ mNewsView?.onNewsItemLoaded(it) }, { mNewsView?.onError(it) })
    }

    fun onDestroy() {
        subscription?.unsubscribe()
    }

}