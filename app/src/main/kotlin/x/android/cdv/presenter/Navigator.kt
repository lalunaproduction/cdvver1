package x.android.cdv.presenter

import android.app.Activity
import android.content.Intent
import android.support.v4.app.ActivityCompat
import x.android.cdv.view.activities.MainActivity
import x.android.cdv.view.activities.screen.DrawerActivity
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by ndvu on 7/10/17.
 */
@Singleton
class Navigator {

    @Inject
    fun Navigator() {
    }

    fun navigateToNews(activity: Activity) {
        val intent = Intent(activity, MainActivity::class.java)
        ActivityCompat.startActivity(activity, intent, null)
    }

    fun navigateToDrawer(activity: Activity) {
        val intent = Intent(activity, DrawerActivity::class.java)
        ActivityCompat.startActivity(activity, intent, null)
    }
}