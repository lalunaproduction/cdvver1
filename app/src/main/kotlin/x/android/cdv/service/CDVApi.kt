package x.android.cdv.service

import x.android.cdv.model.NewsResponse

import retrofit2.http.GET
import rx.Observable

interface CDVApi {

    @GET("/feeds/newsdefaultfeeds.cms?feedtype=sjson")
    fun getNews(): Observable<NewsResponse>
}