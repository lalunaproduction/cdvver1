package x.android.cdv.service.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by ndvu on 7/7/17.
 */


class DataResponse<T> {

    @Expose
    @SerializedName("result")
    var data: T? = null
}