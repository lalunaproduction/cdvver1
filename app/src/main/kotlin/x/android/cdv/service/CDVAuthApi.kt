package x.android.cdv.service

import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import rx.Observable
import x.android.cdv.model.Login
import x.android.cdv.model.User
import x.android.cdv.service.response.DataResponse

/**
 * Created by ndvu on 7/7/17.
 */
interface CDVAuthApi {
    @POST("api/session")
    @Headers("Content-Type : application/json")
    fun login(@Body login : Login): Observable<DataResponse<User>>
}