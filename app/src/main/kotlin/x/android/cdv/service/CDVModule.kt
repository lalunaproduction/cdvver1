package x.android.cdv.service

import x.android.cdv.presenter.NewsPresenter
import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import rx.schedulers.Schedulers
import x.android.cdv.application.Constants


/**
 * Created by VuMap
 */
@Module
class CDVModule {

    @Provides
    @Singleton
    fun provideNewsPresenter(): NewsPresenter {
        return NewsPresenter()
    }

    @Provides
    @Singleton
    internal fun provideNewApiInterface(): CDVApi {
        val retrofit = Retrofit.Builder()
                .baseUrl(Constants.CDV_BASE_API)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .build()
        return retrofit.create(CDVApi::class.java)
    }
}