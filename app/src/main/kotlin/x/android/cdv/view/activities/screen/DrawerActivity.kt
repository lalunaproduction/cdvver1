package x.android.cdv.view.activities.screen

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.activity_drawer.*
import x.android.cdv.R
import x.android.cdv.presenter.widget.Drawer

/**
 * Created by ndvu on 7/12/17.
 */
class DrawerActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drawer)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        prepareDrawer()
        prepareFab()
    }

    private fun prepareFab() {
        fab.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show()
        }
    }

    private fun prepareDrawer() {
        val drawerFragment = fragment_navigation_drawer as Drawer
        drawerFragment.initDrawer(R.id.fragment_navigation_drawer, drawer_layout, toolbar)
    }

    fun setTitleBar(titleBar: CharSequence) {
        title_toolbar.text = titleBar
    }
}