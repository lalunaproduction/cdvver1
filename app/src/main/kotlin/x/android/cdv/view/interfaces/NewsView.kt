package x.android.cdv.view.interfaces

import x.android.cdv.model.NewsItem

interface NewsView {

    fun onNewsItemLoaded(newsItems: List<NewsItem>)

    fun onError(throwable: Throwable?)

    fun hideLoading()

}