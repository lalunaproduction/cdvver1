package x.android.cdv.view.activities.screen

import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import kotlinx.android.synthetic.main.screen_onboarding.*
import x.android.cdv.R
import x.android.cdv.presenter.Navigator
import javax.inject.Inject
import x.android.cdv.application.until.ImageHelper

/**
 * Created by ndvu on 7/10/17.
 */
class OnBoarding : AppCompatActivity() {

    val SPLASH_DISPLAY_LENGTH = 3000
    @Inject
    lateinit var navigator: Navigator
    @Inject
    lateinit var imageHelper: ImageHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.screen_onboarding)
        onScaleViewAndShowAccessOption()
        navigator = Navigator()
        imageHelper = ImageHelper()

        this.scaleDownSizeImage()
        this.onScaleViewAndShowAccessOption()
        this.prepareListener()
    }

    private fun prepareListener() {
        btn_login.setOnClickListener { this.navigateToMain() }
        btn_reg.setOnClickListener { this.navigateToDrawer() }
    }

    private fun navigateToDrawer() {
        navigator.navigateToDrawer(this)
    }

    private fun scaleDownSizeImage() {
        val scaledBitmap = imageHelper.scaleBitmap(resources, R.drawable.logo_cdve, 480)
        image_logo.setImageBitmap(scaledBitmap)

    }

    private fun onScaleViewAndShowAccessOption() {
        Handler().postDelayed({
            zoomOutImage(image_logo, 1f, 0.6f)
            pushFormUp()
        }, SPLASH_DISPLAY_LENGTH.toLong())
    }

    private fun pushFormUp() {
        ll_form.visibility = View.VISIBLE
        val anim = ScaleAnimation(1f, 1f,
                0.1f, 1f,
                Animation.RELATIVE_TO_SELF, 0.6f,
                Animation.RELATIVE_TO_SELF, 1f)
        anim.fillAfter
        anim.duration = 2000
        ll_form.startAnimation(anim)
    }

    private fun zoomOutImage(v: View, startScale: Float, endScale: Float) {
        val anim = ScaleAnimation(1f, 0.6f,
                startScale, endScale,
                Animation.RELATIVE_TO_SELF, 0.6f,
                Animation.RELATIVE_TO_SELF, 0f)
        anim.fillAfter = true
        anim.duration = 2000
        v.startAnimation(anim)


    }

    private fun navigateToMain() {
        navigator.navigateToNews(this)
    }

}