package x.android.cdv.application.until

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by x on 7/10/17.
 */
@Singleton
class ImageHelper {
    @Inject
    fun ImageHelper() {

    }
    fun scaleBitmap(resources: Resources, resId: Int, maximumSize: Int): Bitmap? {
        var image: Bitmap? = null
        try {
            val options = BitmapFactory.Options()
            options.inJustDecodeBounds = true
            BitmapFactory.decodeResource(resources, resId, options)
            options.inScaled = true
            options.inSampleSize = calculateInSampleSize(options, maximumSize, maximumSize)
            options.inJustDecodeBounds = false

            image = BitmapFactory.decodeResource(resources, resId, options)
            System.gc()

        } catch (outOfMemoryError: OutOfMemoryError) {
            outOfMemoryError.printStackTrace()
        }

        return image
    }

    private fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1

        if (height > reqHeight || width > reqWidth) {
            val halfHeight = height / 2
            val halfWidth = width / 2
            while (halfHeight / inSampleSize >= reqHeight && halfWidth / inSampleSize >= reqWidth) {
                inSampleSize *= 2
            }
        }

        return inSampleSize
    }
}