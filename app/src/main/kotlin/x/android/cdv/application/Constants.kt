package x.android.cdv.application

class Constants {
    companion object {
        val NEWS_ENDPOINT = "http://timesofindia.indiatimes.com/"
        val CDV_BASE_API = "https://cdv-2017-leminhy89.c9users.io/"
        val DATE_PATTERN = "MMM dd, yyyy, hh.mma"
        val IST_TIME_ZONE = "IST"
        val LEADING_ZERO_TEMPLATE = "%02d"
    }
}