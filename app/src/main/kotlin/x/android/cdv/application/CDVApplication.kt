package x.android.cdv.application

import android.app.Application
import x.android.cdv.view.activities.MainActivity
import dagger.Component
import io.realm.Realm
import io.realm.RealmConfiguration
import x.android.cdv.service.CDVModule
import javax.inject.Singleton

class CDVApplication : Application() {
    var injector: AppInjector? = null

    @Singleton
    @Component(modules = arrayOf(CDVModule::class))
    interface AppInjector {
        fun inject(activity: MainActivity)
    }

    override fun onCreate() {
        super.onCreate()
        injector = DaggerCDVApplication_AppInjector.builder().build()

        Realm.init(this)
        val config = RealmConfiguration.Builder().build()
        Realm.setDefaultConfiguration(config)
    }
}