package x.android.cdv.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

/**
 * Created by ndvu on 7/7/17.
 */
open class Login : RealmObject() {
    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("password")
    @Expose
    var password: String? = null
}