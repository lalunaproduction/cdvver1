package x.android.cdv.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

/**
 * Created by ndvu on 7/7/17.
 */
open class User : RealmObject() {
    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("name")
    @Expose
    var name: String? = null

    @SerializedName("role_id")
    @Expose
    var role_id: Int? = null

    @SerializedName("email")
    @Expose
    var email: String? = null

    @SerializedName("phone")
    @Expose
    var phone: String? = null

    @SerializedName("token")
    @Expose
    var token: String? = null

    @SerializedName("customer_type_id")
    @Expose
    var customer_type_id: String? = null

    @SerializedName("role")
    @Expose
    var role: Role? = null

    @SerializedName("profile")
    @Expose
    var profile: Profile? = null
}