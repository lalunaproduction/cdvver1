package x.android.cdv.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject

/**
 * Created by ndvu on 7/7/17.
 */
open class Profile : RealmObject() {
    @SerializedName("id")
    @Expose
    var id: Int? = null

    @SerializedName("user_id")
    @Expose
    var user_id: Int? = null

    @SerializedName("full_name")
    @Expose
    var full_name: String? = null

    @SerializedName("address")
    @Expose
    var address: String? = null

    @SerializedName("district")
    @Expose
    var district: String? = null

    @SerializedName("city")
    @Expose
    var city: String? = null
}